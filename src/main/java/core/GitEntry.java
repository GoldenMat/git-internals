package core;

import java.util.Scanner;
import java.util.zip.*;
import java.io.*;

public abstract class GitEntry {

	protected String path;
	protected String hashh;
	protected String hashb;

	public GitEntry(String path, String hash) {
		this.path = path;
		this.hashh = hash.substring(0,2);
		this.hashb = hash.substring(2);
	}

	public String getHash() {
   	  	return hashh + hashb;
	}

	public String getType() throws FileNotFoundException {
		Scanner s = new Scanner(new InflaterInputStream(new FileInputStream(this.path + "/objects/" + hashh + "/" + hashb)));
   	  	return s.next();
        }
}
