package core;

import java.util.Scanner;
import java.util.zip.*;
import java.io.*;

public class GitCommitObject extends GitEntry {

	public GitCommitObject(String path, String hash) {
		super(path,hash);
	}

	public String getTreeHash() throws FileNotFoundException {
		Scanner s = new Scanner(new InflaterInputStream(new FileInputStream(this.path + "/objects/" + hashh + "/" + hashb)));
   	  	s.next();
    	        s.skip("[^\0]*\0");
		s.skip("tree ");
		return s.nextLine();
	}

	public String getParentHash() throws FileNotFoundException {
		Scanner s = new Scanner(new InflaterInputStream(new FileInputStream(this.path + "/objects/" + hashh + "/" + hashb)));
   	  	s.next();
    	        s.skip("[^\0]*\0");
		s.nextLine();
		s.skip("parent ");
		return s.nextLine();
	}

	public String getAuthor() throws FileNotFoundException {
		Scanner s = new Scanner(new InflaterInputStream(new FileInputStream(this.path + "/objects/" + hashh + "/" + hashb)));
   	  	s.next();
    	        s.skip("[^\0]*\0");
		s.nextLine();
		s.nextLine();
		s.skip("author ");
		String author = "";
		for(int i=0;i<2;i++)
			author += s.next()+" ";
		author += s.next();
		return author;
	}

}
