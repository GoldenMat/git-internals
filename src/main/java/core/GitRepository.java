package core;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class GitRepository {

	private String path;

	public GitRepository(String path) {
		this.path = path;
	}

	public String getHeadRef() throws FileNotFoundException {
		Scanner s = new Scanner(new File(this.path + "/HEAD"));
		s.skip("ref:");
		String head = s.next();
		return head;
	}

	public String getRefHash(String ref) throws FileNotFoundException {
		Scanner s = new Scanner(new File(this.path + "/" + ref));
		String hash = s.next();
		return hash;
	}

}
